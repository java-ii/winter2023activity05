import java.nio.file.*;
import java.util.*;

public class ShapeReader {
  public static void main(String[] args) throws Exception {
    Cone codeControl = new Cone(3, 4); 
    System.out.println(codeControl);
    Cone[] cones = loadTriangles("C:\\Users\\Edgar Santos\\Documents\\(2) DawsonCollege\\(2) Computer Science Courses\\(1) Programming\\(2) Programming II\\(1) Labs\\Lab-5\\(2) files\\ShapeProgram\\cones.csv");
    printCones(cones);
    
  }
  
  public static Cone[] loadTriangles(String path) throws Exception {
    List<String> linesAsList = Files.readAllLines(Paths.get(path));
    String[] lines = linesAsList.toArray(new String[0]);
 
    Cone[] cones = new Cone[lines.length];
    for (int i = 0; i < lines.length; i++) {
      String[] pieces = lines[i].split(","); 
      Double d = Double.parseDouble(pieces[0]);
      Double o = Double.parseDouble(pieces[1]);
      cones[i] = new Cone(d, o);
    }
    
    return cones;
  }
  
  public static void printCones(Cone[] cones) {
    for (Cone cone : cones) {
      System.out.println(cone);
    }
  }
}
